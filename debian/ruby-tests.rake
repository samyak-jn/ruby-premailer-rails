require 'gem2deb/rake/spectask'

task :setup do
  Dir.mkdir 'spec/rails_app/tmp'
end

task :run_tests do
  Gem2Deb::Rake::RSpecTask.new do |spec|
   spec.pattern = './spec/**/*_spec.rb'
  end
end

task :cleanup do
  at_exit {
     `rm -rf spec/rails_app/tmp spec/rails_app/log`
  }
end

task :default => [:setup, :run_tests, :cleanup]
